var gulp = require('gulp');
var sass = require('gulp-sass');
var sassGlob = require('gulp-sass-glob');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');

// sass
gulp.task('sass', function() {

    return gulp.src('./sass/*.scss')

        .pipe(sassGlob())
        .pipe(sourcemaps.init())
        .pipe(sass({
            compress: false
        }))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./css'))
  
});

gulp.task('default', gulp.series('sass', function(){
   gulp.watch( './sass/**/*.scss', gulp.series('sass'));
}));
