# STYLEGUIDES

# GULP INSTALL

You need install Node JS (https://nodejs.org/en/) to your computer.

Go to styleguides folder and install npm, if no install in theme.

```
> npm install
```

# GULP TASK

compile styleguides style:
```
> gulp sg
```

compile css files from sass theme to code:
```
> gulp css
```

compile phtml files from sass theme to code:
```
> gulp phtml
```

create base.phtml:
```
> gulp base
```

create atoms.phtml:
```
> gulp atoms
```

create molecules.phtml:
```
> gulp molecules
```

create organisms.phtml:
```
> gulp organisms
```

create layouts.phtml:
```
> gulp layouts
```

create pages.phtml:
```
> gulp pages
```

copy theme images:
```
> gulp images
```

# GULP LIBRARIES

```
gulp
gulp-file
fs
gulp-tap
gulp-replace
path
gulp-header
gulp-sass
gulp-sass-glob
gulp-autoprefixer
gulp-rename
gulp-inject
slash
```
