# HOLY - atomic theme

This a startup theme for your project.

It respects Brad Frost atomic design:\
https://bradfrost.com/blog/post/atomic-web-design/


# DRUPAL WAY

- Holy theme is applicable to frontend project.
- Its based on the Classy theme.
- The goal is to style standard profile installation.


# GULP

You have to install Node JS (https://nodejs.org/en/) to your computer.

a, Go to theme folder, where `package.json` is located and install npm modules to your theme.

```bash
> npm install
```

b, compile sass to css

```bash
> gulp
```

# GULP LIBRARIES

```
"browserslist"
"gulp"
"gulp-autoprefixer"
"gulp-sass"
"gulp-sass-glob"
```

These are the basic npm components for compiling SASS to CSS.


# ATOMIC FOLDER

This folder structure is fundamental.\
It will help you respecting the atomic structure.

```
01-atoms
02-molecules
03-organisms
04-layouts
05-pages
```

